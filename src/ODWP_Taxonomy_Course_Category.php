<?php
/**
 * odwp-courses
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license Mozilla Public License 2.0 https://www.mozilla.org/MPL/2.0/
 * @link https://bitbucket.com/ondrejd/odwp-courses
 * @package odwp-courses
 */

if (!class_exists('ODWP_Taxonomy_Course_Category')):

/**
 * Class implementing course category taxonomy.
 *
 * @since 0.3.0
 */
class ODWP_Taxonomy_Course_Category {
  /**
   * Name of taxonomy.
   * @const string
   */
  const NAME = 'course_category';

  /**
   * Initialize taxonomy.
   *
   * @access public
   * @return void
   * @since 0.3.0
   * @static
   */
  public static function init() {
    $labels = array(
      'name' => _x('Kategorie kurzů', 'taxonomy general name', ODWP_COURSES),
      'singular_name' => _x('Kategorie kurzu', 'taxonomy singular name', ODWP_COURSES),
      'menu_name' => __('Kategorie kurzů', ODWP_COURSES),
      'all_items' => __('Všechny kategorie', ODWP_COURSES),
      'edit_item' => __('Edituj kategorii', ODWP_COURSES),
      'view_item' => __('Zobraz kategorii', ODWP_COURSES),
      'update_item' => __('Aktualizuj kategorii', ODWP_COURSES),
      'add_new_item' => __('Přidej novou kategorii', ODWP_COURSES),
      'parent_item' => __('Nadřazená kategorie', ODWP_COURSES),
      'parent_item_colon' => __('Nadřazená kategorie:', ODWP_COURSES),
      'search_items' => __('Hledat kategorie', ODWP_COURSES),
      'popular_items' => __('Oblíbené kategorie', ODWP_COURSES),
      'separate_items_with_commas' => __('Oddělte kategorie čárkami', ODWP_COURSES),
      'add_or_remove_items' => __('Přidej nebo odstraň kategorie', ODWP_COURSES),
      'choose_from_most_used' => __('Vyberte z nejpoužívanějších kategorií', ODWP_COURSES),
      'not_found' => __('Žádna kategorie nebyla nalezena', ODWP_COURSES)
    );

    $args = array(
      'label' => __('Kategorie kurzů', ODWP_COURSES),
      'labels' => $labels,
      'public' => true,
      'show_in_menu' => true,
      'show_in_nav_menus' => true,
      'show_tagcloud' => false,
      'show_in_quick_edit' => true,
      //'meta_box_cb' => null,
      'show_admin_column' => true,
      'description' => __('Kategorie jednotlivých kurzů', ODWP_COURSES),
      'hierarchical' => true,
      //'update_count_callback' => null
      'rewrite' => true
    );

    require_once (plugin_dir_path(__FILE__) . 'ODWP_Custom_Post_Type_Course.php');

    register_taxonomy(
      self::NAME,
      ODWP_Custom_Post_Type_Course::NAME,
      $args
    );
  } // end init()
} // end ODWP_Taxonomy_Course_Category

endif;