<?php
/**
 * odwp-courses
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license Mozilla Public License 2.0 https://www.mozilla.org/MPL/2.0/
 * @link https://bitbucket.com/ondrejd/odwp-courses
 * @package odwp-courses
 */

if (!class_exists('ODWP_Custom_Post_Type_Course')):

/**
 * Class implementing course custom post type.
 *
 * @since 0.3.0
 */
class ODWP_Custom_Post_Type_Course {
  /**
   * Name of taxonomy.
   * @const string
   */
  const NAME = 'course';

  /**
   * Initialize custom post type.
   *
   * @access public
   * @return void
   * @since 0.3.0
   * @static
   */
  public static function init() {
    $labels = array(
      'name' => _x('RIV RV - Kurzy', 'post type general name', ODWP_COURSES),
      'singular_name' => _x('Vytvořit kurz', 'post type singular name', ODWP_COURSES),
      'add_new' => _x('Přidej nový', 'add new course', ODWP_COURSES),
      'add_new_item' => __('Přidej nový kurz', ODWP_COURSES),
      'edit_item' => __('Edituj kurz', ODWP_COURSES),
      'new_item' => __('Nový kurz', ODWP_COURSES),
      'view_item' => __('Zobraz kurz', ODWP_COURSES),
      'search_items' => __('Prohledej kurzy', ODWP_COURSES),
      'not_found' => __('Žádný kurz nebyl nalezen', ODWP_COURSES),
      'not_found_in_trash' => __('Žádný kurz nebyl v koši nalezen', ODWP_COURSES),
      'all_items' => __('Přehled kurzů', ODWP_COURSES),
      'archives' => __('Archiv kurzů', ODWP_COURSES),
      'menu_name' => __('RIV RV - Kurzy', ODWP_COURSES)
    );

    require_once (plugin_dir_path(__FILE__) . 'ODWP_Taxonomy_Course_Category.php');

    $args = array(
      'labels' => $labels,
      'description' => __('Správa kurzů poskytovaných Richardem Vojtíkem.', ODWP_COURSES),
      'public' => true,
      'menu_position' => 5,
      'menu_icon' => 'dashicons-welcome-learn-more',
      //'capability_type' => 'course',
      //'map_meta_cap' => true,
      'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'comments','revisions','author'),
      'taxonomies' => array('ODWP_Taxonomy_Course_Category::NAME'),
      'has_archive' => true
    );

    register_post_type(self::NAME, $args);
  } // end init()
} // end ODWP_Custom_Post_Type_Course

endif;