<?php
/**
 * odwp-courses
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license Mozilla Public License 2.0 https://www.mozilla.org/MPL/2.0/
 * @link https://bitbucket.com/ondrejd/odwp-courses
 * @package odwp-courses
 */

if (!class_exists('ODWP_Courses')):

/**
 * Main class of the plugin.
 *
 * @since 0.0.1
 */
class ODWP_Courses {
  /**
   * Unique identifier for the plugin.
   * @var string $plugin_slug
   */
  protected $plugin_slug = ODWP_COURSES;

  /**
   * Holds URL to the plugin.
   * @var string $plugin_url
   */
  protected $plugin_url = ODWP_COURSES_URL;

  /**
   * Version of the the plugin.
   * @var string $plugin_version
   */
  protected $plugin_version = ODWP_COURSES_VERSION;

  /**
   * Default options of the plugin.
   * @var array $default_options
   */
  protected $default_options = array(
    // Agreement texts
    'agreement_payment' => '',
    'agreement_course' => ''
  );

  /**
   * Holds instance of class self. Part of singleton implementation.
   * @var ODWP_Courses $instance
   */
  private static $instance;

  /**
   * Returns instance of class self. Part of singleton implementation.
   *
   * @return ODWP_Courses
   */
  public static function get_instance() {
    if (!(self::$instance instanceof ODWP_Courses)) {
      self::$instance = new ODWP_Courses();
    }

    return self::$instance;
  } // end get_instance()

  /**
   * Initializes the plugin.
   *
   * @return void
   * @since 0.0.1
   * @uses add_action()
   * @uses add_filter()
   * @uses register_deactivation_hook()
   * @uses wp_get_theme()
   */
  private function __construct() {
    add_action('init', array($this, 'load_plugin_textdomain'));
    add_action('init', array($this, 'init'));

    if (is_admin()) {
      add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
      add_action('admin_menu', array($this, 'admin_menu'));
      add_action('add_meta_boxes', array($this, 'course_metaboxes'));
      add_action('save_post', array($this, 'save_course_date_metabox'));
      add_action('new_to_publish', array($this, 'save_course_date_metabox'));
      add_action('save_post', array($this, 'save_course_fio_metabox'));
      add_action('new_to_publish', array($this, 'save_course_fio_metabox'));
      add_action('wp_ajax_account_inline_edit', array($this, 'ajax_account_inline_edit'));
      add_filter('manage_course_posts_columns', array($this, 'manage_posts_columns'));
      add_action('manage_posts_custom_column', array($this, 'manage_custom_columns'), 10, 2);
      add_filter('manage_edit-course_sortable_columns', array($this, 'manage_sortable_columns'));
      add_action('restrict_manage_posts', array($this, 'restrict_listings_by_course_category'));
    }
  } // end __construct()

  /**
   * Load the plugin text domain for translation.
   *
   * @return void
   * @since 0.0.1
   * @uses get_locale()
   * @uses load_plugin_textdomain
   */
  public function load_plugin_textdomain() {
    load_plugin_textdomain(
      $this->plugin_slug,
      false,
      $this->plugin_slug . '/languages'
    );
  } // end load_plugin_textdomain

  /**
   * Initialize plugin.
   *
   * @return void
   * @since 0.0.1
   * @uses register_post_type()
   */
  public function init() {
    // Ensure that plugin's options are initialized
    $this->get_options();

    // Initialize our custom taxonomies
    require_once (plugin_dir_path(__FILE__) . 'ODWP_Taxonomy_Course_Category.php');
    ODWP_Taxonomy_Course_Category::init();

    // Initialize our custom post type
    require_once (plugin_dir_path(__FILE__) . 'ODWP_Custom_Post_Type_Course.php');
    ODWP_Custom_Post_Type_Course::init();
  } // end init()

  /**
   * Returns plugin's options
   *
   * @return array
   * @since 0.0.1
   * @uses get_option()
   * @uses update_option()
   */
  public function get_options() {
    $options = get_option($this->plugin_slug . '-options');
    $need_update = false;

    if ($options === false) {
      $need_update = true;
      $options = array();
    }

    foreach ($this->default_options as $key => $value) {
      if (!array_key_exists($key, $options)) {
        $options[$key] = $value;
      }
    }

    if (!array_key_exists('latest_used_version', $options)) {
      $options['latest_used_version'] = $this->plugin_version;
      $need_update = true;
    }

    if($need_update === true) {
      update_option($this->plugin_slug . '-options', $options);
    }

    return $options;
  } // end get_options()

  /**
   * Add plugin's stylesheets and scripts into the WordPress administration.
   *
   * @return void
   * @since 0.2.0
   * @uses wp_register_script()
   * @uses wp_register_style()
   * @uses wp_enqueue_script()
   * @uses wp_enqueue_style()
   */
  public function admin_scripts() {
    wp_register_style(
      $this->plugin_slug . '_admin_css',
      $this->plugin_url . 'css/admin.css',
      false,
      $this->plugin_version
    );

    wp_register_script(
      $this->plugin_slug . '_admin_js',
      $this->plugin_url . 'js/admin.js',
      array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'),
      time(),
      true
    );

    //wp_enqueue_script($this->plugin_slug . '_admin_js');
    //wp_enqueue_style('jquery-ui-datepicker');
    wp_enqueue_style($this->plugin_slug . '_admin_css');
  } // end admin_style()

  /**
   * Add plugin's pages in WordPress administration.
   *
   * @return void
   * @since 0.0.1
   * @uses add_options_page()
   */
  public function admin_menu() {
      add_options_page(
        __('RIS RV - Nastavení', $this->plugin_slug),
        __('RIS RV - Nastavení', $this->plugin_slug),
        'manage_options',
        $this->plugin_slug . '-options',
        array($this, 'admin_options_page')
      );
      add_options_page(
        __('RIS RV - FIO Banka', $this->plugin_slug),
        __('RIS RV - FIO Banka', $this->plugin_slug),
        'manage_options',
        $this->plugin_slug . '-options_fio',
        array($this, 'admin_options_fio_page')
      );
  } // end admin_menu()

  /**
   * Main options page.
   *
   * @return void
   * @since 0.0.1
   */
  public function admin_options_page() {
    $options = $this->get_options();
    $need_update = $updated = false;

    if (
      filter_input(INPUT_POST, $this->plugin_slug . '_submit') &&
      (bool) wp_verify_nonce(filter_input(INPUT_POST, $this->plugin_slug . '_nonce')) === true
    ) {
      $need_update = true;

      $agreement_payment = filter_input(INPUT_POST, $this->plugin_slug . '_agreement_payment');
      $agreement_course  = filter_input(INPUT_POST, $this->plugin_slug . '_agreement_course');

      if (
        $options['agreement_payment'] == $agreement_payment &&
        $options['agreement_course'] == $agreement_course
      ) {
        $need_update = false;
      } else {
        $options['agreement_payment'] = $agreement_payment;
        $options['agreement_course'] = $agreement_course;
      }
    }

    if ($need_update === true) {
      $updated = update_option($this->plugin_slug . '-options', $options);
    }
?>
<div class="wrap">
  <h1><?= __('RIS RV - Nastavení', $this->plugin_slug)?></h1>
  <?php if ($need_update === true && $updated === true):?>
  <div id="<?= $this->plugin_slug?>_message" class="updated notice is-dismissible">
    <p><?= __('Nastavení pluginu bylo úspěšně aktualizováno.', $this->plugin_slug)?></p>
  </div>
  <?php elseif ($need_update === true && $updated !== true):?>
  <div id="<?= $this->plugin_slug?>_message" class="error notice is-dismissible">
    <p><?= __('Nastavení pluginu nebylo úspěšně aktualizováno!', $this->plugin_slug)?></p>
  </div>
  <?php endif?>
  <form name="<?= $this->plugin_slug?>_form" id="<?= $this->plugin_slug?>_form" action="<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_page'))?>" method="post" novalidate>
    <?= wp_nonce_field(-1, $this->plugin_slug . '_nonce', true, false)?>
    <h2 class="title"><?= __('Hlavní nastavení', $this->plugin_slug)?></h2>
    <p><?= __('Hlavní nastavení pluginu <strong>RIV RV - Kurzy</strong>.', $this->plugin_slug);?></p>
    <table class="form-table">
      <tbody>
        <tr>
          <th scope="row">
            <label for="<?= $this->plugin_slug?>_agreement_payment"><?= __('Souhlas s plat. podmínkami', $this->plugin_slug)?></label>
          </th>
          <td>
            <fieldset>
              <p>
                <label for="<?= $this->plugin_slug?>_agreement_payment"><?= __('Tento text bude překopírován k dané objednávce kurzu v momentu jejího vytvoření.', $this->plugin_slug)?></label>
              <textarea name="<?= $this->plugin_slug?>_agreement_payment" id="<?= $this->plugin_slug?>_agreement_payment" value="<?= $options['agreement_payment']?>" class="large-text code" cols="50" rows="10"></textarea>
              </p>
            </fieldset>
          </td>
        </tr>
        <tr>
          <th scope="row">
            <label for="<?= $this->plugin_slug?>_agreement_course"><?= __('Souhlas s podmínkami účasti na kurzu', $this->plugin_slug)?></label>
          </th>
          <td>
            <fieldset>
              <p>
                <label for="<?= $this->plugin_slug?>_agreement_course"><?= __('Tento text bude překopírován k dané objednávce kurzu v momentu jejího vytvoření.', $this->plugin_slug)?></label>
              <textarea name="<?= $this->plugin_slug?>_agreement_course" id="<?= $this->plugin_slug?>_agreement_course" value="<?= $options['agreement_course']?>" class="large-text code" cols="50" rows="10"></textarea>
              </p>
            </fieldset>
          </td>
        </tr>
      </tbody>
    </table>
    <p class="submit">
      <input type="submit" value=" <?php echo __('Uložit změny', $this->plugin_slug)?> " name="<?= $this->plugin_slug . '_submit'?>" class="button button-primary">
    </p>
  </form>
</div>
<?php
  } // end admin_options_page()

  /**
   * FIO Bank options page.
   *
   * @return void
   * @since 0.2.0
   * @uses wp_verify_nonce()
   * @uses wp_nonce_field()
   */
  public function admin_options_fio_page() {
    global $wpdb;

    require_once (dirname(__FILE__) . '/ODWP_Bank_Account.php');

    $filter = filter_input(INPUT_GET, 'filter');
    if (empty($filter) || !in_array($filter, array('all', 'deleted'))) {
      $filter = 'all';
    }

    $need_insert = $inserted = false;
    $insert_msg = '';

    // Adding new account
    if (
      filter_input(INPUT_POST, $this->plugin_slug . '_submit_new') &&
      (bool) wp_verify_nonce(filter_input(INPUT_POST, $this->plugin_slug . '_nonce')) === true
    ) {
      $need_insert = true;
      $_number = filter_input(INPUT_POST, $this->plugin_slug . '_number');
      $_token = filter_input(INPUT_POST, $this->plugin_slug . '_token');
      $number = trim($_number);
      $token = trim($_token);

      if (empty($number)) {
        $insert_msg = __('Nejde uložit účet s nezadaným číslem účtu!', $this->plugin_slug);
      }
      else if (!is_null(ODWP_Bank_Account::findByAccount($number))) {
        $insert_msg = __('Účet s daným číslem již existuje! Účet nelze uložit!', $this->plugin_slug);
      }
      else {
        $account = new ODWP_Bank_Account();
        $account->number = $number;
        $account->token = $token;
        $account->save();

        if ($account->id > 0) {
          $insert_msg = __('Nový účet byl úspěšně uložen!', $this->plugin_slug);
          $inserted = true;
        }
        else {
          $insert_msg = __('Nový účet nebyl úspěšně uložen!', $this->plugin_slug);
          $inserted = false;
        }
      }
    }

    $need_delete = $deleted = false;
    $delete_msg = '';

    // Set single deleted
    if (filter_input(INPUT_GET, 'delete')) {
      $need_delete = true;
      $account_id = (int) filter_input(INPUT_GET, 'delete');
      $account = ODWP_Bank_Account::findById($account_id);

      if (($account instanceof ODWP_Bank_Account)) {
        $account->deleted = 1;
        if (true === $account->save()) {
          $deleted = true;
          $delete_msg = sprintf(__('Účet <code>%s</code> byl úpěšně označen jako smazaný!', $this->plugin_slug), $account->number);
        } else {
          $deleted = false;
          $delete_msg = sprintf(__('Účet <code>%s</code> nebyl označen jako smazaný!', $this->plugin_slug), $account->number);
        }
      } else {
        $deleted = false;
        $delete_msg = sprintf(__('Účet s ID <code>%d</code> nebyl nalezen! Žádný účet nebyl označen jako smazaný.', $this->plugin_slug), $account_id);
      }
    }

    $need_restore = $restored = false;
    $restore_msg = '';

    // Restore single
    if (filter_input(INPUT_GET, 'restore')) {
      $need_restore = true;
      $account_id = (int) filter_input(INPUT_GET, 'restore');
      $account = ODWP_Bank_Account::findById($account_id);

      if (($account instanceof ODWP_Bank_Account)) {
        $account->deleted = 0;
        if (true === $account->save()) {
          $restored = true;
          $restore_msg = sprintf(__('Účet <code>%s</code> byl úpěšně obnoven a nyní již není smazaný!', $this->plugin_slug), $account->number);
        } else {
          $restored = false;
          $restore_msg = sprintf(__('Účet <code>%s</code> nebyl úspěšně obnoven!', $this->plugin_slug), $account->number);
        }
      } else {
        $restored = false;
        $restore_msg = sprintf(__('Účet s ID <code>%d</code> nebyl nalezen! Žádný účet nebyl obnoven.', $this->plugin_slug), $account_id);
      }
    }

    // Removing/Restoring multiple
    $need_action = $action_executed = false;
    $action_msg = '';

    $action = '';
    $action1 = filter_input(INPUT_POST, 'action');
    $action2 = filter_input(INPUT_POST, 'action2');

    if (in_array($action1, array('trash', 'restore'))) {
      $action = $action1;
    }
    else if (in_array($action2, array('trash', 'restore'))) {
      $action = $action2;
    }

    if (in_array($action, array('trash', 'restore'))) {
      $need_action = true;
      $account_ids = filter_input(INPUT_POST, 'account', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

      if (is_array($account_ids)) {
        if (count($account_ids) > 0) {
          foreach ($account_ids as $account_id) {
            $account = ODWP_Bank_Account::findById($account_id);
            $account->deleted = ($action == 'trash') ? 1 : 0;
            $account->save();
          }

          $action_executed = true;
          $action_msg = __('Zvolená akce byla úspěšně provedena.', $this->plugin_slug);
        }
        else {
          $action_executed = false;
          $action_msg = __('Pro zvolenou akci nebyly určeny žádné účty.', $this->plugin_slug);
        }
      }
      else {
        $action_executed = false;
        $action_msg = __('Pro zvolenou akci nebyly určeny žádné účty.', $this->plugin_slug);
      }
    }

    $accounts = ODWP_Bank_Account::findAll();
    $deleted_accounts = ODWP_Bank_Account::findByFilter(array('deleted' => 1));
    $displayed_accounts = ($filter == 'deleted') ? $deleted_accounts : $accounts;
?>
<div class="wrap">
  <img src="<?= $this->plugin_url?>/img/fio-logo.png" alt="<?= __('FIO Banka', $this->plugin_slug)?>" class="alignright">
  <h1 class="clear: both;"><?= __('RIV RV - FIO Banka', $this->plugin_slug)?></h1>
  <?php if ($need_insert === true && $inserted === true):?>
  <div id="<?= $this->plugin_slug?>_message" class="updated notice is-dismissible">
    <p><?= $insert_msg?></p>
  </div>
  <?php elseif ($need_insert === true && $inserted !== true):?>
  <div id="<?= $this->plugin_slug?>_message" class="error notice is-dismissible">
    <p><?= $insert_msg?></p>
  </div>
  <?php endif;?>
  <?php if ($need_delete === true && $deleted === true):?>
  <div id="<?= $this->plugin_slug?>_message" class="updated notice is-dismissible">
    <p><?= $delete_msg?></p>
  </div>
  <?php elseif ($need_delete === true && $deleted !== true):?>
  <div id="<?= $this->plugin_slug?>_message" class="error notice is-dismissible">
    <p><?= $delete_msg?></p>
  </div>
  <?php endif;?>
  <?php if ($need_restore === true && $restored === true):?>
  <div id="<?= $this->plugin_slug?>_message" class="updated notice is-dismissible">
    <p><?= $restore_msg?></p>
  </div>
  <?php elseif ($need_restore === true && $restored !== true):?>
  <div id="<?= $this->plugin_slug?>_message" class="error notice is-dismissible">
    <p><?= $restore_msg?></p>
  </div>
  <?php endif;?>
  <?php if ($need_action === true && $action_executed === true):?>
  <div id="<?= $this->plugin_slug?>_message" class="updated notice is-dismissible">
    <p><?= $action_msg?></p>
  </div>
  <?php elseif ($need_action === true && $action_executed !== true):?>
  <div id="<?= $this->plugin_slug?>_message" class="error notice is-dismissible">
    <p><?= $action_msg?></p>
  </div>
  <?php endif;?>
  <p><?= __('Zde můžete nastavit dostupné účty u <strong>FIO banky</strong>. Nový účet můžete přidat pomocí formuláře <a href="#new-account-title">níže</a>.', $this->plugin_slug)?></p>
  <h2 class="title" id="existing-accounts-title"><?= __('Existující účty', $this->plugin_slug)?></h2>
  <div class="wrap">
    <form name="<?= $this->plugin_slug?>_form" id="<?= $this->plugin_slug?>_form" action="<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_fio&filter=' . $filter))?>" method="post" novalidate>
      <h3 class="screen-reader-text"><?= __('Filtrovat účty diamantů', $this->plugin_slug)?></h3>
      <ul class="subsubsub">
        <li class="all">
          <a<?= ($filter == 'all') ? ' class="current"' : ''?> href="<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_fio&filter=all'))?>">
            <?= __('Celkem', $this->plugin_slug)?>
            <span class="count">(<?= count($accounts)?>)</span>
          </a>
          |
        </li>
        <li class="deleted">
          <a<?= ($filter == 'deleted') ? ' class="current"' : ''?> href="<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_fio&filter=deleted'))?>">
            <?= __('Smazané', $this->plugin_slug)?>
            <span class="count">(<?= count($deleted_accounts)?>)</span>
          </a>
        </li>
      </ul>
      <div class="tablenav top">
        <div class="alignleft actions bulkactions">
          <label class="screen-reader-text" for="bulk-action-selector-top"><?= __('Zvolit akci pro hromadné úpravy', $this->plugin_slug)?></label>
          <select id="bulk-action-selector-top" name="action">
            <option value="-1"><?= __('Hromadné úpravy', $this->plugin_slug)?></option>
            <?php if ($filter != 'deleted'):?>
            <option value="trash"><?= __('Označit jako smazané', $this->plugin_slug)?></option>
            <?php else:?>
            <option value="restore"><?= __('Zrušit stav smazaný', $this->plugin_slug)?></option>
            <?php endif?>
          </select>
          <input id="doaction" class="button action" type="submit" value="<?= __('Použít', $this->plugin_slug)?>">
        </div>
        <div class="tablenav-pages one-page">
          <span class="displaying-num"><?= sprintf(__('Položek: %d', $this->plugin_slug), count($displayed_accounts))?></span>
        </div>
      </div>
      <table id="the-list" class="wp-list-table widefat fixed striped accounts">
        <thead>
          <tr>
            <td id="cb" class="manage-column column-cb check-column">
              <label class="screen-reader-text" for="cb-select-all-1"><?= __('Označit vše', $this->plugin_slug)?></label>
              <input type="checkbox" id="cb-select-all-1">
            </td>
            <th id="number" class="manage-column column-number column-primary" scope="col"><?= __('Č. účtu', $this->plugin_slug)?></th>
            <th id="token" class="manage-column column-token column-primary" scope="col"><?= __('Bezpeč. token', $this->plugin_slug)?></th>
            <th id="deleted" class="manage-column column-deleted" scope="col"><?= __('Smazáno', $this->plugin_slug)?></th>
          </tr>
        </thead>
        <tbody>
          <?php if (count($displayed_accounts) > 0):?>
          <?php foreach ($displayed_accounts as $account):?><!-- class="iedit author-self level-0 post-132 type-post status-publish format-standard hentry category-blog" -->
          <tr id="account-<?= $account->id?>" class="hentry format-standard">
            <th class="check-column" scope="row">
              <label class="screen-reader-text" for="cb-select-<?= $account->id?>"><?= sprintf(__('Zvolit položku: %s', $this->plugin_slug), $account->number)?></label>
              <input type="checkbox" id="cb-select-<?= $account->id?>" name="account[]" value="<?= $account->id?>">
            </th>
            <td class="number column-number has-row-actions column-primary page-title" data-colname="<?= __('Č. účtu', $this->plugin_slug)?>">
              <strong><?= $account->number?></strong>
              <div class="row-actions">
                <span class="edit">
                  <a href="#account-<?= $account->id?>" title="<?= __('Upravit tuto položku', $this->plugin_slug)?>" data-id="<?= $account->id?>" data-number="<?= $account->number?>" data-token="<?= $account->token?>" data-deleted="<?= $account->deleted?>" class="edit-account-link"><?= __('Upravit', $this->plugin_slug)?></a>
                  |
                </span>
                <?php if ((int) $account->deleted != 1):?>
                <span class="trash">
                  <a href="<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_fio&filter=' . $filter . '&delete=' . $account->id))?>" title="<?= __('Označit tuto položku jako smazanou.', $this->plugin_slug)?>"><?= __('Smazat', $this->plugin_slug)?></a>
                </span>
                <?php else:?>
                <span class="trash">
                  <a href="<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_fio&filter=' . $filter . '&restore=' . $account->id))?>" title="<?= __('Obnovit tuto smazanou položku.', $this->plugin_slug)?>"><?= __('Obnovit', $this->plugin_slug)?></a>
                </span>
                <?php endif?>
              </div>
            </td>
            <td class="token column-token has-row-actions column-primary page-title" data-colname="<?= __('Bezpeč. token', $this->plugin_slug)?>">
              <code><?= $account->token?></code>
            </td>
            <td class="deleted column-deleted" data-colname="<?= __('Smazáno', $this->plugin_slug)?>">
              <?php if ((int) $account->deleted == 1):?>
              <span style="color: #F00;"><?= __('Ano', $this->plugin_slug)?></span>
              <?php else:?>
              <span style="color: #6C3;"><?= __('Ne', $this->plugin_slug)?></span>
              <?php endif?>
            </td>
          </tr>
          <?php endforeach?>
          <?php else:?>
          <tr>
            <td colspan="4"><?= __('Prozatím nejsou vytvořeny žádné účty&hellip;', $this->plugin_slug)?></td>
          </tr>
          <?php endif;?>
        </tbody>
        <tfoot>
          <tr>
            <td id="cb2" class="manage-column column-cb check-column">
              <label class="screen-reader-text" for="cb-select-all-2"><?= __('Označit vše', $this->plugin_slug)?></label>
              <input type="checkbox" id="cb-select-all-2">
            </td>
            <th id="number2" class="manage-column column-number column-primary" scope="col"><?= __('Č. účtu', $this->plugin_slug)?></th>
            <th id="token2" class="manage-column column-token column-primary" scope="col"><?= __('Bezpeč. token', $this->plugin_slug)?></th>
            <th id="deleted2" class="manage-column column-deleted" scope="col"><?= __('Smazáno', $this->plugin_slug)?></th>
          </tr>
        </tfoot>
      </table>
      <div class="tablenav bottom">
        <div class="alignleft actions bulkactions">
          <label class="screen-reader-text" for="bulk-action-selector-bottom"><?= __('Zvolit akci pro hromadné úpravy', $this->plugin_slug)?></label>
          <select id="bulk-action-selector-bottom" name="action2">
            <option value="-1"><?= __('Hromadné úpravy', $this->plugin_slug)?></option>
            <?php if ($filter != 'deleted'):?>
            <option value="trash"><?= __('Označit jako smazané', $this->plugin_slug)?></option>
            <?php else:?>
            <option value="restore"><?= __('Zrušit stav smazaný', $this->plugin_slug)?></option>
            <?php endif?>
          </select>
          <input id="doaction2" class="button action" type="submit" value="<?= __('Použít', $this->plugin_slug)?>">
        </div>
        <div class="tablenav-pages one-page">
          <span class="displaying-num"><?= sprintf(__('Položek: %d', $this->plugin_slug), count($displayed_accounts))?></span>
        </div>
      </div>
    </form>
  </div>
  <?php // TODO Move into standalone JS file?>
  <script type="text/javascript">
jQuery(document).ready(function() {
  jQuery("#cb-select-all-1,#cb-select-all-2").on("click", function() {
    jQuery(".check-column input[type='checkbox']").prop("checked", jQuery(this).prop("checked"));
  });

  jQuery(".edit-account-link").on("click", function() {
    var account = {
      "id": jQuery(this).data("id"),
      "number": jQuery(this).data("number"),
      "token": jQuery(this).data("token"),
      "deleted": jQuery(this).data("deleted")
    }

    jQuery("#account-" + account.id)
      .css("display", "none")
      .after(
        '<tr id="edit-' + account.id +'" class="inline-edit-row inline-edit-row-post quick-edit-row quick-edit-row-post inline-editor">' +
          '<td colspan="4">' +
            '<fieldset class="inline-edit-col-left">' +
              '<legend class="inline-edit-legend"><?= __('Rychlé úpravy', $this->plugin_slug)?></legend>' +
              '<div class="inline-edit-col">' +
                '<label class="alignleft">' +
                  '<span class="title"><?= __('Č. účtu', $this->plugin_slug)?></span>' +
                  '<span class="input-text-wrap">' +
                    '<input type="text" value="' + account.number + '" name="account_number">' +
                  '</span>' +
                '</label>' +
                '<span class="alignleft inline-edit-or">/2010</span>' +
                '<label class="clear">' +
                  '<span class="title"><?= __('Bezpeč. token', $this->plugin_slug)?></span>' +
                  '<span class="input-text-wrap">' +
                    '<input type="text" value="' + account.token + '" name="account_token">' +
                  '</span>' +
                '</label>' +
              '</div>' +
            '</fieldset>' +
            '<fieldset class="inline-edit-col-center">' +
              '&nbsp;<br>&nbsp;' +
            '</fieldset>' +
            '<fieldset class="inline-edit-col-right">' +
              '<div class="inline-edit-col">' +
                '<div class="inline-edit-group">' +
                  '<br><label class="alignleft">' +
                    '<input type="checkbox" name="account_deleted"' + ((parseInt(account.deleted) == 1) ? ' checked="checked"' : '') + '>' +
                    '<span class="checkbox-title"><?= __('Označit jako smazané?', $this->plugin_slug)?></span>' +
                  '</label>' +
                '</div>' +
              '</div>' +
            '</fieldset>' +
            '<p class="submit inline-edit-save">' +
              '<button class="button-secondary cancel alignleft" type="button"><?= __('Zrušit', $this->plugin_slug)?></button>' +
              '<button class="button-primary save alignright" type="button"><?= __('Aktualizovat', $this->plugin_slug)?></button>' +
              '<span class="spinner"></span>' +
              '<span class="error"></span>' +
              '<br class="clear">' +
            '</p>' +
          '</td>' +
        '</tr>');

    jQuery("#edit-" + account.id + " .save").on("click", function() {
      jQuery("#edit-" + account.id + " .spinner").css("visibility", "visible");
      var new_number = jQuery("#edit-" + account.id + " input[name='account_number']").val();
      var new_token = jQuery("#edit-" + account.id + " input[name='account_token']").val();
      var new_deleted = jQuery("#edit-" + account.id + " input[name='account_deleted']").prop("checked") ? 1 : 0;
      var data = {
        "action": "account_inline_edit",
        "id": account.id,
        "number": new_number,
        "token": new_token,
        "deleted": new_deleted
      };
      jQuery.post(
        "<?= admin_url('admin-ajax.php')?>",
        data,
        function(aResult) {
          jQuery("#edit-" + account.id + " .spinner").delay(100).css("visibility", "visible");
          if (aResult) {
            if (aResult.result) {
              if (aResult.result == 'success') {
                jQuery("#account-" + account.id + " td.number strong").html(new_number);
                jQuery("#account-" + account.id + " td.token code").html(new_token);
                jQuery("#account-" + account.id + " td.number .edit-account-link").data("number", new_number);
                jQuery("#account-" + account.id + " td.token .edit-account-link").data("token", new_token);
                jQuery("#account-" + account.id + " td.number .edit-account-link").data("deleted", new_deleted);
                if (account.deleted != new_deleted) {
                  if (new_deleted != 1) {
                    jQuery("#account-" + account.id + " td.deleted").
                      html('<span style="color: #6C3;"><?= __('Ne', $this->plugin_slug)?></span');
                    jQuery("#account-" + account.id + " td.number span.trash a").
                      attr("href", "<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_fio&filter=' . $filter . '&delete='))?>" + account.id).
                      attr("title", "<?= __('Označit tuto položku jako smazanou.', $this->plugin_slug)?>").
                      html("<?= __('Smazat', $this->plugin_slug)?>");
                  }
                  else {
                    jQuery("#account-" + account.id + " td.deleted").
                      html('<span style="color: #F00;"><?= __('Ano', $this->plugin_slug)?></span');
                    jQuery("#account-" + account.id + " td.number span.trash a").
                      attr("href", "<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_fio&filter=' . $filter . '&restore='))?>" + account.id).
                      attr("title", "<?= __('Obnovit tuto smazanou položku.', $this->plugin_slug)?>").
                      html("<?= __('Obnovit', $this->plugin_slug)?>");
                  }
                }
                jQuery("#account-" + account.id).css("display", "table-row");
                jQuery("#edit-" + account.id + " .cancel").off("click");
                jQuery("#edit-" + account.id + " .save").off("click");
                jQuery("#edit-" + account.id).remove();
                return;
              }
              else {
                jQuery("#edit-" + account.id + " td p.submit span.error").html(aResult.message);
                return;
              }
            }
          }

          jQuery("#edit-" + account.id + " td p.submit span.error").
            html("<?= __('Při ukládání změn došlo k neznámé chybě!', $this->plugin_slug)?>");
        }
      );
    });

    jQuery("#edit-" + account.id + " .cancel").on("click", function() {
      jQuery("#edit-" + account.id + " .cancel").off("click");
      jQuery("#edit-" + account.id + " .save").off("click");
      jQuery("#account-" + account.id).css("display", "table-row");
      jQuery("#edit-" + account.id).remove();
    });
  });
});
  </script>
  <h2 class="title" id="new-account-title"><?= __('Nový účet', $this->plugin_slug)?></h2>
  <p><?= __('Zadejte nový bankovní účet.', $this->plugin_slug);?></p>
  <form name="<?= $this->plugin_slug?>_form" id="<?= $this->plugin_slug?>_form" action="<?= esc_url(admin_url('admin.php?page=' . $this->plugin_slug . '-options_fio&filter=' . $filter))?>" method="post" novalidate>
    <?= wp_nonce_field(-1, $this->plugin_slug . '_nonce', true, false)?>
    <table class="form-table">
      <tbody>
        <tr>
          <th scope="row">
            <label for="<?= $this->plugin_slug?>_number"><?= __('Číslo účtu', $this->plugin_slug)?></label>
          </th>
          <td>
            <input type="text" name="<?= $this->plugin_slug?>_number" id="<?= $this->plugin_slug?>_number" value="" length="255">/2010<br>
            <span class="description"><?= __('Zadejte plné číslo účtu včetně předčíslí. Kód banky bude doplněn automaticky.', $this->plugin_slug)?></span>
          </td>
        </tr>
        <tr>
          <th scope="row">
            <label for="<?= $this->plugin_slug?>_token"><?= __('Bezpeč. token', $this->plugin_slug)?></label>
          </th>
          <td>
            <input type="text" name="<?= $this->plugin_slug?>_token" id="<?= $this->plugin_slug?>_token" value="" length="255" class="regular-text"><br>
            <span class="description"><?= __('Bezpečnostní token k vašemu bankovnímu účtu získáte v internetovém bankovnictví <strong>FIO banky</strong>.', $this->plugin_slug)?></span>
          </td>
        </tr>
      </tbody>
    </table>
    <p class="submit">
      <input type="submit" value=" <?php echo __('Přidat účet', $this->plugin_slug)?> " name="<?= $this->plugin_slug . '_submit_new'?>" class="button button-primary">
    </p>
  </form>
</div>
<?php
  } // end admin_options_fio_page()

  /**
   * Handles Ajax calls when is submitted inline form for editing shapes.
   *
   * @return void
   * @since 0.0.1
   * @todo Improve security using `wp_nonce`!
   * @uses sanitize_text_field()
   * @uses wp_die()
   */
  public function ajax_account_inline_edit() {
    require_once (dirname(__FILE__) . '/ODWP_Bank_Account.php');

    $reponse = array();
    $account_id     = filter_input(INPUT_POST, 'id');
    $account_number = filter_input(INPUT_POST, 'number');
    $account_token  = filter_input(INPUT_POST, 'token');
    $shape_deleted  = filter_input(INPUT_POST, 'deleted');

    if (empty($account_id)) {
      $response['result'] = 'failure';
      $response['message'] = __('Dotaz neobsahuje správná data!', $this->plugin_slug);
    }
    else {
      $shape = ODWP_Bank_Account::findById((int) $account_id);

      if (!($shape instanceof ODWP_Bank_Account)) {
        $response['result'] = 'failure';
        $response['message'] = __('Účet k aktualizaci nebyl nalezen!', $this->plugin_slug);
      }
      else {
        $shape->number = sanitize_text_field($account_number);
        $shape->token = sanitize_text_field($account_token);
        $shape->deleted = (intval($shape_deleted) >= 1) ? 1 : 0;

        if ($shape->save() === true) {
          $response['result'] = 'success';
          $response['message'] = __('Účet byl úspěšně aktualizován.', $this->plugin_slug);
        }
        else {
          $response['result'] = 'failure';
          $response['message'] = __('Účet nebyl úspěšně aktualizován.', $this->plugin_slug);
        }
      }
    }

    header('Content-Type: application/json');
    echo json_encode($response);
    wp_die();
  } // end ajax_account_inline_edit()

  /**
   * Add meta boxes for courses.
   *
   * @return void
   * @since 0.1.0
   * @uses add_meta_box()
   */
  public function course_metaboxes() {
    add_meta_box(
      'course_date_metabox',
      __('Datum konání', $this->plugin_slug),
      array($this, 'course_metabox_date'),
      'course',
      'side',
      'low'
    );
    add_meta_box(
      'course_fio_metabox',
      __('Bankovní účet', $this->plugin_slug),
      array($this, 'course_metabox_fio'),
      'course',
      'side',
      'low'
    );
  } // end course_date_metabox()

  /**
   * Renders content for `course_date_metabox`.
   *
   * @param WP_Post $post
   * @return void
   * @since 0.1.0
   * @uses wp_nonce_field()
   */
  public function course_metabox_date($post) {
    $term_from = get_post_meta($post, 'course_term_from', true);
    $term_to   = get_post_meta($post, 'course_term_to', true);

    wp_nonce_field(ODWP_COURSES_FILE, 'course_date_metabox_content_nonce');
?>
<fieldset class="course-date-metabox">
  <p>
    <label for="course_term_from"><?= __('Datum od:', $this->plugin_slug)?></label>
    <?php odwpc_touch_time($term_from, 0, $this->plugin_slug, '1');?>
  </p>
  <p>
    <label for="course_term_to"><?= __('Datum do:', $this->plugin_slug)?></label>
    <?php odwpc_touch_time($term_to, 0, $this->plugin_slug, '2');?>
  </p>
</fieldset>
<?php
  } // end course_metabox_date($post)

  /**
   * Renders content for `course_fio_metabox`.
   *
   * @param WP_Post $post
   * @return void
   * @since 0.1.0
   * @uses wp_nonce_field()
   */
  public function course_metabox_fio($post) {
    require_once (dirname(__FILE__) . '/ODWP_Bank_Account.php');

    $accounts = ODWP_Bank_Account::findAll();
    $account_id = get_post_meta($post, 'course_bank_account_id', true);

    wp_nonce_field(ODWP_COURSES_FILE, 'course_fio_metabox_content_nonce');
?>
<fieldset>
  <p>
    <label for="course_fio_bank_account"><?= __('Bankovní účet:', $this->plugin_slug)?></label>
    <select name="course_fio_bank_account" id="course_fio_bank_account" value="<?= $account_id?>">
    <?php foreach ($accounts as $account):?>
      <option value="<?= $account->id?>"<?= selected($account_id, $account->id)?>><?= $account->number?></option>
    <?php endforeach?>
    </select><span>/2010</span>
  </p>
</fieldset>
<?php
  } // end course_metabox_fio($post)

  /**
   * Saves values from `course_date_metabox`.
   *
   * @param integer $post_id
   * @return void
   * @since 0.3.0
   * @uses add_post_meta()
   * @uses current_uses_can()
   * @uses delete_post_meta()
   * @uses get_post_type_object()
   * @uses get_post_meta()
   * @uses update_post_meta()
   * @uses wp_verify_nonce()
   */
  public function save_course_date_metabox($post_id) {
    // verify nonce
    $nonce = filter_input(INPUT_POST, 'course_date_metabox_content_nonce');
    if (!wp_verify_nonce($nonce, ODWP_COURSES_FILE)) {
      return/* 'nonce not verified'*/;
    }

    // Get values
    $term_from = odwpc_get_touch_time_data($this->plugin_slug, '1');
    $term_to   = odwpc_get_touch_time_data($this->plugin_slug, '2');

    // Save values
    $this->update_meta_key($post_id, 'course_term_from', $term_from);
    $this->update_meta_key($post_id, 'course_term_to', $term_to);
  } // end save_course_date_metabox($post_id)

  /**
   * Saves values from `course_fio_metabox`.
   *
   * @param integer $post_id
   * @return void
   * @since 0.3.0
   */
  public function save_course_fio_metabox($post_id) {
    // verify nonce
    $nonce = filter_input(INPUT_POST, 'course_fio_metabox_content_nonce');
    if (!wp_verify_nonce($nonce, ODWP_COURSES_FILE)) {
      return/* 'nonce not verified'*/;
    }

    // Get value
    $bank_account = filter_input(INPUT_POST, 'course_fio_bank_account');

    // Save value
    $this->update_meta_key($post_id, 'course_bank_account_id', $bank_account);
  } // end save_course_fio_metabox($post_id)

  /**
   * Change the columns for the courses table.
   *
   * @param array $cols
   * @return array
   * @since 0.3.0
   */
  public function manage_posts_columns($cols) {
    require_once (plugin_dir_path(__FILE__) . 'ODWP_Taxonomy_Course_Category.php');

    $cols = array(
      'cb' => '<input type="checkbox">',
      'title' => __('Název kurzu', $this->plugin_slug),
      'term' => __('Termín konání (od/do)', $this->plugin_slug),
      ODWP_Custom_Post_Type_Course::NAME => __('Kategorie', $this->plugin_slug)
    );

    return $cols;
  } // end manage_posts_columns($cols)

  /**
   * Add content for our custom columns for the courses table.
   *
   * @param string $column
   * @param integer $post_id
   * @return void
   * @since 0.3.0
   */
  public function manage_custom_columns($column, $post_id) {
    require_once (plugin_dir_path(__FILE__) . 'ODWP_Taxonomy_Course_Category.php');

    switch ($column) {
      case 'term':
        $from = get_post_meta($post_id, 'course_term_from', true);
        $to   = get_post_meta($post_id, 'course_term_to', true);

        printf(
          '<span class="term_from_to"><span class="from">%s</span>/<span class="to">%s</span></span>',
          date('j.n.Y H:i', strtotime($from)),
          date('j.n.Y H:i', strtotime($to))
        );
        break;

      case ODWP_Taxonomy_Course_Category::NAME:
        // TODO Finish this!
        echo '<em>&ndash;&ndash;&ndash;</em>';
        break;
    }
    //
  } // end manage_custom_columns($column, $post_id)

  /**
   * Make our columns in courses table sortable.
   *
   * @return array
   * @since 0.3.0
   */
  public function manage_sortable_columns() {
    return array(
      'term' => 'term',
      'title' => 'title',
      ODWP_Custom_Post_Type_Course::NAME => ODWP_Custom_Post_Type_Course::NAME
    );
  } // end manage_sortable_columns()

  /**
   * @access private
   * @param integer $post_id
   * @param string $meta_key
   * @param $string $value
   * @return void
   * @since 0.3.0
   * @uses add_post_meta()
   * @uses update_post_meta()
   * @uses delete_post_meta()
   */
  private function update_meta_key($post_id, $meta_key, $value) {
    $current_value = get_post_meta($post_id, $meta_key, true);

    if ($value && '' == $current_value) {
      add_post_meta($post_id, $meta_key, $value, true);
    }
    elseif ($value && $value != $current_value) {
      update_post_meta($post_id, $meta_key, $value);
    }
    elseif ('' == $value && $current_value) {
      delete_post_meta($post_id, $meta_key, $current_value);
    }
  } // end update_meta_key($post_id, $meta_key, $value)

  /**
   * Hook for `restrict_manage_posts` action (for courses listing).
   *
   * @global string $typenow
   * @global WP_Query $wp_query
   * @return void
   * @since 0.3.0
   * @uses get_taxonomy()
   * @uses wp_dropdown_categories()
   */
  public function restrict_listings_by_course_category() {
    global $typenow;
    global $wp_query;

    require_once (plugin_dir_path(__FILE__) . 'ODWP_Taxonomy_Course_Category.php');

    if ($typenow == 'course') {
      $taxonomy = ODWP_Taxonomy_Course_Category::NAME;
      $category_taxonomy = get_taxonomy($taxonomy);

      $selected_category = array_key_exists('course_category', $wp_query->query)
          ? $wp_query->query['course_category']
          : null;

      wp_dropdown_categories(array(
          'show_option_all' =>  __('Zobrazit všechny kategorie', $this->plugin_slug),
          'taxonomy'        =>  $taxonomy,
          'name'            =>  $taxonomy,
          'orderby'         =>  'name',
          'selected'        =>  $selected_category,
          'hierarchical'    =>  true,
          'depth'           =>  3,
          'show_count'      =>  true,
          'hide_empty'      =>  true
      ));
    }
  } // end restrict_listing_by_course_category()
} // End of ODWP_Courses

endif;
