<?php
/**
 * odwp-courses
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license Mozilla Public License 2.0 https://www.mozilla.org/MPL/2.0/
 * @link https://bitbucket.com/ondrejd/odwp-wc-diamonds
 * @package odwp-wc-diamonds
 */

if (!class_exists('ODWP_Course')):

/**
 * Model class for courses.
 *
 * @since 0.3.0
 */
class ODWP_Course {
  /**
   * @const string Holds our database table name.
   */
  const TABLE_NAME = 'bank_accounts';

  /**
   * @var integer $id
   */
  public $id;

  /**
   * @var string $number
   */
  public $number;

  /**
   * @var string $token
   */
  public $token;

  /**
   * @var integer $deleted
   */
  public $deleted;

  /**
   * Constructor.
   *
   * @access public
   * @param array $data
   * @return void
   * @since 0.2.0
   */
  public function __construct(array $data = array()) {
    if (array_key_exists('id', $data)) {
      $this->id = $data['id'];
    }

    if (array_key_exists('number', $data)) {
      $this->number = $data['number'];
    }

    if (array_key_exists('token', $data)) {
      $this->token = $data['token'];
    }

    if (array_key_exists('deleted', $data)) {
      $this->deleted = $data['deleted'];
    }
  } // end __construct(array $data = array())

  /**
   * Save entity into the database.
   *
   * @access public
   * @global wpdb $wpdb
   * @return boolean
   * @since 0.2.0
   */
  public function save() {
    global $wpdb;

    $table_name = $wpdb->prefix . ODWP_Bank_Account::TABLE_NAME;

    if ((int) $this->id <= 0) {
      $wpdb->insert(
        $table_name,
        array(
          'number'  => $this->number,
          'token'   => $this->token,
          'deleted' => (int) $this->deleted
        ),
        array('%s', '%s', '%d')
      );

      $this->id = $wpdb->insert_id;

      return ((int) $this->id > 0);
    }

    $res = $wpdb->update(
      $table_name,
      array(
        'number'  => $this->number,
        'token'   => $this->token,
        'deleted' => (int) $this->deleted
      ),
      array('id'  => $this->id),
      array('%s', '%s', '%d'),
      array('%d')
    );

    return ($res !== false);
  } // end save()

  /**
   * Create our table.
   *
   * @access public
   * @global wpdb $wpdb
   * @return void
   * @since 0.2.0
   * @static
   * @uses dbDelta()
   */
  public static function createTable() {
    global $wpdb;

    // Create our database table if needed
    $table_name = $wpdb->prefix . ODWP_Bank_Account::TABLE_NAME;
    $charset_collate = $wpdb->get_charset_collate();

    if ($wpdb->get_var('SHOW TABLES LIKE "'.$table_name.'" ') != $table_name) {
      $sql = <<<EOT
CREATE TABLE `$table_name` (
`id` INTEGER ( 20 ) NOT NULL AUTO_INCREMENT ,
`number` VARCHAR ( 255 ) NOT NULL ,
`token` VARCHAR ( 255 ) NOT NULL ,
`deleted` INTEGER ( 1 ) NOT NULL DEFAULT 0 COMMENT "Rovno 1, pokud je typ účet označen jako smazaný." ,
PRIMARY KEY `id` ( `id` )
) $charset_collate;
EOT;

      require_once ABSPATH . 'wp-admin/includes/upgrade.php';
      dbDelta($sql);
    }
  } // end createTable()

  /**
   * Find all entities.
   *
   * @access public
   * @global wpdb $wpdb
   * @param boolean $deleted (Optional.) If TRUE returns also items marked as deleted.
   * @return array
   * @since 0.2.0
   * @static
   */
  public static function findAll($deleted = false) {
    global $wpdb;

    $table_name = $wpdb->prefix . ODWP_Bank_Account::TABLE_NAME;
    $sql = "SELECT * FROM `$table_name` WHERE 1 ";

    if ($deleted === false) {
      $sql .= ' AND ( `deleted` IS NULL OR `deleted` = 0 ) ';
    }

    $arr = $wpdb->get_results($sql);
    $ret = array();

    if (!is_array($arr)) {
      return $ret;
    }

    foreach ($arr as $obj) {
      $shape = new ODWP_Bank_Account();
      $shape->id      = (int) $obj->id;
      $shape->number  = $obj->number;
      $shape->token   = $obj->token;
      $shape->deleted = (int) $obj->deleted;
      $ret[] = $shape;
    }

    return $ret;
  } // end findAll($deleted)

  /**
   * Find entity by its ID.
   *
   * @access public
   * @global wpdb $wpdb
   * @param integer $id
   * @return mixed
   * @since 0.2.0
   * @static
   */
  public static function findById($id) {
    global $wpdb;

    $table_name = $wpdb->prefix . ODWP_Bank_Account::TABLE_NAME;
    $sql = "SELECT * FROM `$table_name` WHERE `id` = %d ";
    $obj = $wpdb->get_row($wpdb->prepare($sql, $id));

    if (!is_object($obj)) {
      return null;
    }

    $shape = new ODWP_Bank_Account();
    $shape->id      = (int) $obj->id;
    $shape->number  = $obj->number;
    $shape->token   = $obj->token;
    $shape->deleted = (int) $obj->deleted;

    return $shape;
  } // end findById($id)

  /**
   * Find entity by number column.
   *
   * @access public
   * @global wpdb $wpdb
   * @param string $number
   * @return mixed
   * @since 0.2.0
   * @static
   */
  public static function findByAccount($number) {
    global $wpdb;

    $table_name = $wpdb->prefix . ODWP_Bank_Account::TABLE_NAME;
    $sql = "SELECT * FROM `$table_name` WHERE `number` = %s ";
    $obj = $wpdb->get_row($wpdb->prepare($sql, $number));

    if (!is_object($obj)) {
      return null;
    }

    $shape = new ODWP_Bank_Account();
    $shape->id      = (int) $obj->id;
    $shape->number  = $obj->number;
    $shape->token   = $obj->token;
    $shape->deleted = (int) $obj->deleted;

    return $shape;
  } // end findByAccount($number)

  /**
   * Find entities matching given filter.
   *
   * @access public
   * @global wpdb $wpdb
   * @param array $filter
   * @return array
   * @since 0.2.0
   * @static
   */
  public static function findByFilter($filter = array()) {
    global $wpdb;

    $table_name = $wpdb->prefix . ODWP_Bank_Account::TABLE_NAME;
    $sql = "SELECT * FROM `$table_name` WHERE 1 ";

    if (array_key_exists('number', $filter)) {
      $sql .= 'AND `number` LIKE "' . $filter['name'] . '"';
    }

    if (array_key_exists('token', $filter)) {
      $sql .= 'AND `token` LIKE "' . $filter['name'] . '"';
    }

    if (array_key_exists('deleted', $filter)) {
      $sql .= 'AND `deleted` = "' . $filter['deleted'] . '"';
    }

    $arr = $wpdb->get_results($sql);
    $ret = array();

    if (!is_array($arr)) {
      return $ret;
    }

    foreach ($arr as $obj) {
      $shape = new ODWP_Bank_Account();
      $shape->id      = (int) $obj->id;
      $shape->number  = $obj->number;
      $shape->token   = $obj->token;
      $shape->deleted = (int) $obj->deleted;
      $ret[] = $shape;
    }

    return $ret;
  } // end findByFilter($filter)
} // End of ODWP_Bank_Account

endif;
