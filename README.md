# Plugin `RIV RV - Kurzy`

Plugin pro [WordPress](https://wordpress.org), který umožňuje vytváření, správu a prodej kurzů.

## Hlavní vlastnosti

- administrační část
- _front-end_ část pro zákazníky

## Administrační část

### Přehled kurzů

![Přehled kurzů](screenshot-1.png)

### Přidat nový kurz

![Přidat nový kurz](screenshot-2.png)

### Nastavení

Setting bank accounts:

![Nastavení - FIO Banka](screenshot-3.png)

Other settings:

![Nastavení - Obecné](screenshot-4.png)
