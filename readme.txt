=== RIV RV - Kurzy ===
Contributors: Ondřej Doněk
Donate link: https://bitbucket.org/ondrejd/odwp-courses
License: Mozilla Public License 2.0
License URI: https://www.mozilla.org/MPL/2.0/
Tags: wordpress,post type,eshop
Requires at least: 4.3
Tested up to: 4.4.2
Stable tag: 0.2.0

Plugin pro [WordPress](https://wordpress.org/), který umožňuje vytváření, správu a prodej výukových kurzů.


== Description ==

Plugin pro [WordPress](https://wordpress.org/), který umožňuje vytváření, správu a prodej výukových kurzů.

Hlavní vlastnosti:

* administrační část
* front-end část pro zákazníky


== Installation ==

Plugin nainstalujete následujícím způsobem:

1. [Stáhněte si](https://bitbucket.org/ondrejd/odwp-courses/downloads) plugin z repozitáře
1. Rozbalte plugin do složky `odwp-courses` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in [WordPress](https://wordpress.org/)
3. For other details see [plugin's home page](https://bitbucket.org/ondrejd/odwp-courses)


== Frequently Asked Questions ==

= Question #1 =

Answer #1 ...


== Screenshots ==

1. `screenshot-1.png`
2. `screenshot-2.png`


== License ==

This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at https://mozilla.org/MPL/2.0/.


== Changelog ==

= 0.3.0 =
* finished new post type `course`

= 0.2.0 =
* added bank accounts options page
* added stylesheet for [WordPress](https://wordpress.org/) administration

= 0.1.0 =
* add new post type `course`

= 0.0.1 =
* initial public version
* source codes added to GitHub


