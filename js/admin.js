/**
 * odwp-courses
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @license Mozilla Public License 2.0 https://www.mozilla.org/MPL/2.0/
 * @link https://bitbucket.com/ondrejd/odwp-courses
 */

jQuery(document).ready(function(){
  jQuery(".datepicker").datepicker(); 
});
