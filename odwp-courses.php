<?php
/**
 * Plugin Name: RIV RV - Kurzy
 * Plugin URI: https://bitbucket.org/ondrejd/odwp-courses
 * Description: Plugin pro WordPress, který umožňuje vytváření, správu a prodej kurzů.
 * Version: 0.3.0
 * Author: Ondřej Doněk
 * Author URI: http://ondrejdonek.blogspot.cz/
 * Requires at least: 4.3
 * Tested up to: 4.3.1
 *
 * Text Domain: odwp-courses
 * Domain Path: /languages/
 *
 * @author Ondřej Doněk, <ondrejd@gmail.com>
 * @link https://bitbucket.org/ondrejd/odwp-courses for the canonical source repository
 * @license https://www.mozilla.org/MPL/2.0/ Mozilla Public License 2.0
 * @package odwp-courses
 */


if (!defined('WPINC')) {
	die;
}


defined('ODWP_COURSES') || define('ODWP_COURSES', 'odwp-courses');
defined('ODWP_COURSES_FILE') || define('ODWP_COURSES_FILE', __FILE__);
defined('ODWP_COURSES_URL') || define('ODWP_COURSES_URL', plugin_dir_url(__FILE__));
defined('ODWP_COURSES_VERSION') || define('ODWP_COURSES_VERSION', '0.3.0');


if (!function_exists('odwpc_activate')):

/**
 * Activates the plugin.
 * 
 * @internal
 * @return void
 * @since 0.0.1
 */
function odwpc_activate() {
  require_once (plugin_dir_path(__FILE__) . 'src/ODWP_Bank_Account.php');
  ODWP_Bank_Account::createTable();
} // end odwpc_activate()

endif;


if (!function_exists('odwpc_deactivate')):

/**
 * Deactivates the plugin.
 * 
 * @internal
 * @return void
 * @since 0.0.1
 */
function odwpc_deactivate() {
  // ...
} // end odwpc_deactivate()

endif;


if (!function_exists('odwpc_deactivate_raw')):

/**
 * Deactivates plugin directly by updating WP option `active_plugins`.
 *
 * @internal
 * @link https://developer.wordpress.org/reference/functions/deactivate_plugins/
 * @return void
 * @since 0.1.0
 */
function odwpc_deactivate_raw() {
  $plugins = get_option('active_plugins');
  $out = array();
  foreach($plugins as $key => $val) {
    if($val != ODWP_COURSES.'/'.ODWP_COURSES.'.php') {
      $out[$key] = $val;
    }
  }
  update_option('active_plugins', $out);
} // end odwpc_deactivate_raw()

endif;


if (!function_exists('odwpc_touch_time')):

/**
 * Print out HTML form date and time elements. Created with WordPress's {@see touch_time()} in mind.
 *
 *
 * @global WP_Locale $wp_locale
 * @param string $date (Optional.) Date in `Y-m-d H:i:s` format. If NULL or empty current date is used.
 * @param int $tab_index (Optional.)
 * @param string $name_prefix (Optional.)
 * @param string $name_suffix (Optional.)
 * @return void
 * @since 0.3.0
 */
function odwpc_touch_time($date = null, $tab_index = 0, $name_prefix = '', $name_suffix = '') {
  global $wp_locale;

  $jj = $mm = $aa = $hh = $mn = $ss = null;

  if (empty($date)) {
    $date = current_time('timestamp');
    $jj = gmdate('d', $date);
    $mm = gmdate('m', $date);
    $aa = gmdate('Y', $date);
    $hh = gmdate('H', $date);
    $mn = gmdate('i', $date);
    $ss = gmdate('s', $date);
  }
  else {
    $jj = mysql2date('d', $date, false);
    $mm = mysql2date('m', $date, false);
    $aa = mysql2date('Y', $date, false);
    $hh = mysql2date('H', $date, false);
    $mn = mysql2date('i', $date, false);
    $ss = mysql2date('s', $date, false);
  }

  $tab_index_attribute = '';
  if ((int) $tab_index > 0) {
    $tab_index_attribute = ' tabindex="' . $tab_index . '"';
  }
?>
<div class="timestamp-wrap">
  <label>
    <span class="screen-reader-text"><?= __('Day')?></span>
    <input type="text" id="<?= $name_prefix?>-jj-<?= $name_suffix?>" name="<?= $name_prefix?>-jj-<?= $name_suffix?>" value="<?= $jj?>" size="2" maxlength="2" class="day"<?= $tab_index_attribute?> autocomplete="off">
  </label>.
  <label>
    <span class="screen-reader-text"><?= __('Month')?></span>
    <select id="<?= $name_prefix?>-mm-<?= $name_suffix?>" name="<?= $name_prefix?>-mm-<?= $name_suffix?>" class="month"<?= $tab_index_attribute?>>
      <?php for ($i = 1; $i < 13; $i = $i + 1):
        $monthnum = zeroise($i, 2);
        $monthtext = $wp_locale->get_month_abbrev($wp_locale->get_month($i));
      ?>
      <option value="<?= $monthnum?>" data-text="<?= $monthtext?>" <?php selected($monthnum, $mm, false)?>>
        <?= sprintf(__('%1$s-%2$s'), $monthnum, $monthtext)?>
      </option>
      <?php endfor?>
    </select>
  </label>
  <label>
    <span class="screen-reader-text"><?= __('Year')?></span>
    <input type="text" id="<?= $name_prefix?>-aa-<?= $name_suffix?>" name="<?= $name_prefix?>-aa-<?= $name_suffix?>" value="<?= $aa?>" size="4" maxlength="4" class="year"<?= $tab_index_attribute?> autocomplete="off" class="year">
  </label> @ 
  <label>
    <span class="screen-reader-text"><?= __('Hour')?></span>
    <input type="text" id="<?= $name_prefix?>-hh-<?= $name_suffix?>" name="<?= $name_prefix?>-hh-<?= $name_suffix?>" value="<?= $hh?>" size="2" maxlength="2"<?= $tab_index_attribute?> autocomplete="off" class="hour">
  </label>
  <label>
    <span class="screen-reader-text"><?= __('Minute')?></span>
    <input type="text" id="<?= $name_prefix?>-mn-<?= $name_suffix?>" name="<?= $name_prefix?>-mn-<?= $name_suffix?>" value="<?= $mn?>" size="2" maxlength="2"<?= $tab_index_attribute?> autocomplete="off" class="minute">
  </label>
</div>
<input type="hidden" id="<?= $name_prefix?>-ss-<?= $name_suffix?>" name="<?= $name_prefix?>-ss-<?= $name_suffix?>" value="<?= $ss?>" class="second">
<?php
} // end odwpc_touch_time($date = null, $tab_index = 0, $name_prefix = '', $name_suffix = '')

endif;


if (!function_exists('odwpc_get_touch_time_data')):

/**
 * Return date from form created by {@see odwpc_touch_time} function.
 *
 * @param string $name_prefix (Optional.)
 * @param string $name_suffix (Optional.)
 * @param boolean $is_post (Optional.)
 * @return mixed Returns datetime in `Y-m-d H:i:s` format or NULL.
 * @see odwpc_touch_time()
 * @since 0.3.0
 */
function odwpc_get_touch_time_data($name_prefix = '', $name_suffix = '', $is_post = true) {
  $jj = filter_input(($is_post) ? INPUT_POST : INPUT_GET, $name_prefix . '-jj-' . $name_suffix);
  $mm = filter_input(($is_post) ? INPUT_POST : INPUT_GET, $name_prefix . '-mm-' . $name_suffix);
  $aa = filter_input(($is_post) ? INPUT_POST : INPUT_GET, $name_prefix . '-aa-' . $name_suffix);
  $hh = filter_input(($is_post) ? INPUT_POST : INPUT_GET, $name_prefix . '-hh-' . $name_suffix);
  $mn = filter_input(($is_post) ? INPUT_POST : INPUT_GET, $name_prefix . '-mn-' . $name_suffix);
  $ss = filter_input(($is_post) ? INPUT_POST : INPUT_GET, $name_prefix . '-ss-' . $name_suffix);

  if (empty($jj) || empty($mm) || empty($aa) || empty($hh) || empty($mn) || empty($ss)) {
    return null;
  }

  $date = $aa . '-' . $mm . '-' . $jj . ' ' . $hh . ':' . $mn . ':' . $ss;

  return $date;
} // end odwpc_get_touch_time_data($name_prefix = '', $name_suffix = '', $is_post = true)

endif;


// Everything is OK - initialize the plugin
require_once (plugin_dir_path(__FILE__) . 'src/ODWP_Courses.php');

add_action('plugins_loaded', array('ODWP_Courses', 'get_instance'));
register_activation_hook(ODWP_COURSES_FILE, 'odwpc_activate');
register_deactivation_hook(ODWP_COURSES_FILE, 'odwpc_deactivate');
